extends Node2D

var IP_CLIENT
var PORT_CLIENT
var PORT_SERVER = 19909
var IP_SERVER = "127.0.0.1"
var socketUDP = PacketPeerUDP.new()

func _ready():
	start_server()

func _process(delta):   

	if socketUDP.get_available_packet_count() > 0:
		var array_bytes = socketUDP.get_packet()
		var IP_CLIENT = socketUDP.get_packet_ip()
		var PORT_CLIENT = socketUDP.get_packet_port()
		printt("msg server: " + str(array_bytes))
#		socketUDP.set_dest_address(IP_CLIENT, PORT_CLIENT)
#		var stg = "hi server!"
#		var pac = stg.to_ascii()
#		socketUDP.put_packet(pac)

func print_bytearray(bytes):
	for i in range(bytes.size()):
		print(bytes[i])
	

func start_server():
	if (socketUDP.listen(PORT_SERVER) != OK):
		printt("Error listening on port: " + str(PORT_SERVER))
	else:
		printt("Listening on port: " + str(PORT_SERVER))

func _exit_tree():
	socketUDP.close()