extends Node2D

onready var peer = PacketPeerUDP.new()

func _ready():
	peer.listen(9909)

func _process(delta):
	while peer.get_available_packet_count() > 0:
		var data = Array(peer.get_packet())
		var offset = 0
		var nick = ""
		if data[offset] != 1:
			print("ERROR: Invalid light packet version")
			return
		offset += 1
		while offset < data.size():
			if data[offset] == 0:
				# nick tag
				var nick_array = PoolByteArray()
				for i in range(offset + 1, data.size()):
					if data[i] == 0:
						break
					nick_array.append(data[i])
				nick = nick_array.get_string_from_ascii()
				offset += len(nick) + 2
			elif data[offset] == 1:
				# light tag
				var light_index = data[offset+1]
				var color = Color(
					data[offset+3]/255.0,
					data[offset+4]/255.0,
					data[offset+5]/255.0
				)
				set_light_color(light_index, color)
				offset += 6
		
func set_light_color(i, color):
	if i < $Lights.get_child_count():
		$Lights.get_children()[i].modulate = color
	else:
		print("ERROR: Invalid light index: ", i)

func _exit_tree():
	peer.close()