# Instanssivalo

![Kuvankaappaus](screenshot.png)

Huvin vuoksi tehty valopalvelin, joka näyttää samalla valot sijaintineen. Kaikki on hieman kovakoodattua, koska tämä oli lähinnä testailua eikä tarkoitettu julkaistavaksi. Sen vuoksi voi olla hieman buginenkin.

Kuuntelee kovakoodatusti UDP-porttia 9909, joten ei voi pitää päällä samaan aikaan effectserverin kanssa.

Vaatii Godot 3.0.6:n. https://godotengine.org/

Projektin saa ajettua suorittamalla Godotin ja avaamalla projektin eli tässä kansiossa olevan project.godot -tiedoston. Myös Godotin ajaminen komentoriviltä onnistuu.

Salin pohjakarttakuva on instanssi.org -sivustolta.
